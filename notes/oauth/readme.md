Dependencies : Flask-Dance, Flask-SQLAlchemy, Flask-Login, blinker, flask_wtf

To support HTTP :

export OAUTHLIB_INSECURE_TRANSPORT=1

To start the application :
python -m flask run
