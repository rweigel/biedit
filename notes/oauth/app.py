from flask import Flask
from flask import flash
import requests, os, shutil
from flask_dance.contrib.github import make_github_blueprint, github
from flask import Flask, render_template, request, redirect, url_for, session
from flask_sqlalchemy import SQLAlchemy
from flask_dance.contrib.github import make_github_blueprint
from flask_login import UserMixin, current_user, LoginManager, login_required, login_user, logout_user
from flask_dance.consumer.storage.sqla import OAuthConsumerMixin, SQLAlchemyStorage
from flask_dance.consumer import oauth_authorized, oauth_error
from sqlalchemy.orm.exc import NoResultFound
import subprocess
from forms import RepoForm
from collections import namedtuple

app = Flask(__name__)

app.secret_key = "56695862"
blueprint = make_github_blueprint(
    client_id="afb4cb9b88d7c6ede452",
    client_secret="671bb9b1abed8036a29072a0951961c67237292c",
)

app.register_blueprint(blueprint, url_prefix="/github_login")

app.config['SQLALCHEMY_DATABASE_URI']='sqlite:////Users/srikar_reddy/Downloads/ksr4599_Hapi/flask-dance/login.db'
db = SQLAlchemy(app)

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(256), unique=True)

class OAuth(OAuthConsumerMixin, db.Model):
    provider_user_id = db.Column(db.String(256), unique=True)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    user = db.relationship(User)

login_manager = LoginManager(app)
login_manager.login_view = 'github.login'



@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

blueprint.backend = SQLAlchemyStorage(OAuth, db.session, user=current_user)

@oauth_authorized.connect_via(blueprint)
def github_logged_in(blueprint, token):
    if not token:
        flash("Failed to log in with GitHub.", category="error")
        return False

    resp = blueprint.session.get("/user")
    if not resp.ok:
        msg = "Failed to fetch user info from GitHub."
        flash(msg, category="error")
        return False

    github_info = resp.json()
    github_user_id = str(github_info["id"])

    # Find this OAuth token in the database, or create it
    query = OAuth.query.filter_by(
        provider=blueprint.name,
        provider_user_id=github_user_id,
    )
    try:
        oauth = query.one()
    except NoResultFound:
        oauth = OAuth(
            provider=blueprint.name,
            provider_user_id=github_user_id,
            token=token,
        )

    if oauth.user:
        login_user(oauth.user)
        user_data = OAuth.query.get(int(oauth.user.id))
        print("user_oauth_token")
        user_oauth_token = user_data.token["access_token"]
        print(user_oauth_token)

        print("Successfully signed in with GitHub.")

    else:
        user = User(
            username=github_info["login"]
        )
        oauth.user = user
        db.session.add_all([user, oauth])
        db.session.commit()
        login_user(user)
        print("Successfully signed in with GitHub.")

    return False

# notify on OAuth provider error
@oauth_error.connect_via(blueprint)
def github_error(blueprint, error, error_description=None, error_uri=None):
    msg = (
        "OAuth error from {name}! "
        "error={error} description={description} uri={uri}"
    ).format(
        name=blueprint.name,
        error=error,
        description=error_description,
        uri=error_uri,
    )
    flash(msg, category="error")


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route("/", methods=['GET', 'POST'])
def index():
    form = RepoForm()
    git_stream = ""
    git_stream_error = ""
    if form.is_submitted():
        result = request.form
        print("result : ")
        print(result['repo_name'])

        #Empty the dir (I guess there are better options to delete all files, dir, sub-dir inside a folder)
        dir = os.path.abspath(os.getcwd()) + "/temp_repos"
        dir1 = os.path.abspath(os.getcwd()) + "/templates"
        for files in os.listdir(dir):
            path = os.path.join(dir, files)
            try:
                shutil.rmtree(path)
            except OSError:
                os.remove(path)
        #Executing the git clone inside temp_repos
        subprocess.call("ls", cwd=dir1)
        user_data = OAuth.query.get(int(current_user.id))
        user_oauth_token = user_data.token["access_token"]
        print(user_oauth_token)

        cmd = "cd temp_repos && git clone --progress https://"+ user_oauth_token + "@github.com/"+ current_user.username + "/" + result['repo_name'] + ".git"
        print("cmd")
        print(cmd)
        stream = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        tuple = stream.communicate()
        stream.wait()
        git_stream = tuple[0]
        git_stream_error = tuple[1]

    return render_template("home.html", form=form, git_stream= git_stream, git_stream_error = git_stream_error)


# hook up extensions to app'
db.init_app(app)
login_manager.init_app(app)

if __name__ == "__main__":
    if "--setup" in sys.argv:
        with app.app_context():
            db.create_all()
            db.session.commit()
            print("Database tables created")
    else:
        app.run(debug=True)
