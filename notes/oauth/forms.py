#Forms
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField

class RepoForm(FlaskForm):
    repo_name = StringField('repo_name')
    submit = SubmitField('submit')
