Plus sign gets interpreted as bullet.

$$
\boldsymbol{\nabla}\cdot\mathbf{A} =
{1 \over r^2}{\partial \left( r^2 A_r \right) \over \partial r}
+ {1 \over r\sin\theta}{\partial \over \partial \theta} \left(  A_\theta\sin\theta \right)
+ {1 \over r\sin\theta}{\partial A_\phi \over \partial \phi}
$$
