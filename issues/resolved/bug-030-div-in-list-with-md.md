10. Should be indented. And no `\htmlnewline`.

   <div>
   *a*
   </div>
   
11. Should not be indented.

  <div>
  *a*
  </div>