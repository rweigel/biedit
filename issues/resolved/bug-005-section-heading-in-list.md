* a b c
# A
This section heading appears in list and as a result, HTML wrapping of section is wrong. If a newline is added before heading line, rendering is correct.
