from setuptools import setup, find_packages

install_requires = ["psutil","pyppeteer"]

setup(
    name='biedit',
    version='0.0.1',
    author='Bob Weigel',
    author_email='rweigel@gmu.edu',
    packages=find_packages(),
    url='https://gitlab.com/rweigel/biedit',
    license='LICENSE.txt',
    description='Markdown editor',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    install_requires=install_requires
)
