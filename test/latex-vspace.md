# `[3em]` stand-alone

top

\\[3em]

5em lower

# `[3em]` as part of paragraph

**abaaa**
\\[3em]
_a_

# `\vspace` with `div`

A vertical space of 3em with a div:

<div style="height:3em"/>

New p.

# `\vspace`

A vertical space of 3em with `\vspace`

\vspace{3em}

New p.