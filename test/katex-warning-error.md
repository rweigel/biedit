
$$
I(t) = \frac{V_s}{R}(1-e^{-t/(L/R)}) = \frac{20 \text{ V}}{2~\Omega} \left(1-e^{-(10\text{ ms})/(20\text{ ms})}\right) = 3.9\text{ A} 
$$

$$
emf = V_s - IR= 20\text{ V} - 3.9\text{ A} \cdot 2~\Omega=12.2\text{ V} 
$$


$$
I(t) =  \frac{V_s}{R} (1-e^{-t/\tau})=10 \times (1-0.368) = 6.32\text{ A}
$$

$$
I(t)=\frac{V_s}{R}\left(1-e^{-t/(L/R)}\right)
$$

For large $t$, the exponential term is near zero, leaving $V_s/R=$10~V/1 $\Omega$ = $10$~A.

$$
-I(t) R - L\frac{dI(t)}{dt} = 0
$$


$$
-I(t) \cdot R - L \frac{dI(t)}{dt} = 0
$$

And using $I=10e^{-t/\tau}$ and $d e^{t/\tau}/dt=(1/\tau) e^{-t/\tau}$, we have

$$
-(10\text{ A})e^{-t/\tau} \cdot (1\Omega) - (10\text{ mH}/\tau)e^{-t/\tau} = 0
$$

Using $\tau = L/R$ one arrives at

$$
-(10\text{ V})e^{-t/\tau} - (10\text{ V})e^{-t/\tau} = 0
$$
