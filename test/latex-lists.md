1. A
2. B

1. A
   A

   B
   1. a
      a

      \ifsolutions
      $true$
      \else
      $false$
      \fi

      b

2. A
   A

   B
   1. a
      a

      b
   2. a
      b

     <div>
      <img src="latex-from-html.svg" alt="Grid" style="width:20%"/>
     </div>
