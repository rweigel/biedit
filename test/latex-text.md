# Usage

a \textbf{x} b

\underline{abc    } \texttt{abc} sdfsdf

# Spacing `bf` et al.

1. a c (nothing between a and c)

2. a {\bf} c (one hair space)

3. a {\bf } c (one hair space)

4. a { \bf} c (one full space)

5. a { \bf } c (space after bf replaced with full space)

6. a {\bf b} c (shift for bold)

7. a {\bf b } c (space replaced with nbsp)

8. a {\bf b  } c (last space replaced with nbsp)

9. a { \bf b} c (full space added before b)

10. a { \bf b } c (spaces replaced with full space)

11. a {  \bf b  } c (first and last spaces replaced with full space)

12. a {   \bf b   } c (full space added before and after b)

13. a {   \bf} c (full and thin space added before and after b)


# Spacing `textbf` et al.

1. a b

2. a \textbf{} b

3. a \textbf{a} b

4. a \textbf{a } b

5. a \textbf{a  } b

6. a \textbf{ a} b

7. a \textbf{  a} b

8. a \textbf{ a } b

9. a \textbf{  a  } b
